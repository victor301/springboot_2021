package es.edu.alten.practica0.modelo;

public class Lagarto extends PiedraPapelTijeraFactory{

	private Lagarto(String nombre, int numero) {
		super(nombre, numero);
	}
	
	public Lagarto(String nombre) {
		super(nombre, OptionsDefined.LAGARTO.getId());
	}
	
	public Lagarto() {
		this(OptionsDefined.LAGARTO.getName(),OptionsDefined.LAGARTO.getId());
	}

	@Override
	public boolean isMe(int numero) {
		return LAGARTO == numero;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory piedraPapelTijeraFactory) {
		int result = 0;
		
		switch (piedraPapelTijeraFactory.getNumero()) {
		
		case PAPEL:
		case SPOCK:
			result = 1;
			descripcionResultado = "Lagarto le gano a " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case PIEDRA:
		case TIJERA:
			result = -1;
			descripcionResultado = "Lagarto perdio contra " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case LAGARTO:
			result = 0;
			descripcionResultado = "Lagarto empato contra " + piedraPapelTijeraFactory.getNombre();
			break;
			
		}
		
		return result;
	}

}
