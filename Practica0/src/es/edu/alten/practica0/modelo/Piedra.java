package es.edu.alten.practica0.modelo;

public class Piedra extends PiedraPapelTijeraFactory{

	private Piedra(String nombre, int numero) {
		super(nombre, numero);
	}
	
	public Piedra(String nombre) {
		this(nombre,OptionsDefined.PIEDRA.getId());
	}
	
	public Piedra() {
		this(OptionsDefined.PIEDRA.getName(),OptionsDefined.PIEDRA.getId());
	}

	@Override
	public boolean isMe(int numero) {
		return PIEDRA == numero;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory piedraPapelTijeraFactory) {
		int result = 0;
		
		switch (piedraPapelTijeraFactory.getNumero()) {
		
		case TIJERA:
		case LAGARTO:
			result = 1;
			descripcionResultado = "Piedra le gano a " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case PAPEL:
		case SPOCK:
			result = -1;
			descripcionResultado = "Piedra perdio contra " + piedraPapelTijeraFactory.getNombre();
			break;
		
		case PIEDRA:
			result = 0;
			descripcionResultado = "Piedra empato contra " + piedraPapelTijeraFactory.getNombre();
			break;
		}
		
		return result;
	}

}
