package es.edu.alten.practica0.modelo;

public class Spock extends PiedraPapelTijeraFactory{

	private Spock(String nombre, int numero) {
		super(nombre, numero);
	}
	
	public Spock(String nombre) {
		this(nombre,OptionsDefined.SPOCK.getId());
	}
	
	public Spock() {
		this(OptionsDefined.SPOCK.getName(),OptionsDefined.SPOCK.getId());
	}

	@Override
	public boolean isMe(int numero) {
		return SPOCK == numero;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory piedraPapelTijeraFactory) {
		int result = 0;
		
		switch (piedraPapelTijeraFactory.getNumero()) {
		
		case PIEDRA:
		case TIJERA:
			result = 1;
			descripcionResultado = "Spock le gano a " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case PAPEL:
		case LAGARTO:
			result = -1;
			descripcionResultado = "Spock perdio contra " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case SPOCK:
			result = 0;
			descripcionResultado = "Spock empato contra " + piedraPapelTijeraFactory.getNombre();
			break;
			
		}
		
		return result;
	}

}
