package es.edu.alten.practica0.modelo;

public enum OptionsDefined {

	PIEDRA(1,"PIEDRA"),
	PAPEL(2,"PAPEL"),
	TIJERA(3,"TIJERA"),
	LAGARTO(4,"LAGARTO"),
	SPOCK(5,"SPOCK");
	
	private int id;
	private String name;
	
	private OptionsDefined (int id, String name){
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
