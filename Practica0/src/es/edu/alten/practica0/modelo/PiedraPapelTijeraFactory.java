package es.edu.alten.practica0.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {

	public final static int PIEDRA = 1;
	public final static int PAPEL = 2;
	public final static int TIJERA = 3;
	public final static int LAGARTO = 4;
	public final static int SPOCK = 5;
	
	protected String descripcionResultado;
	private static List<PiedraPapelTijeraFactory> elementos;
	protected String nombre;
	protected int numero;
	
	public PiedraPapelTijeraFactory (String nombre, int numero) {
		this.nombre = nombre;
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public void setDescripcionResultado(String descripcionResultado) {
		this.descripcionResultado = descripcionResultado;
	}
	
	public abstract boolean isMe(int numero);
	
	public abstract int comparar (PiedraPapelTijeraFactory piedraPapelTijeraFactory);
	
	public static PiedraPapelTijeraFactory getInstance (int numero) {
		
		PiedraPapelTijeraFactory factory = null;
		
		elementos = new ArrayList<>();
		
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		elementos.add(new Lagarto());
		elementos.add(new Spock());
		
		for (PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			if (piedraPapelTijeraFactory.isMe(numero)) {
				factory = piedraPapelTijeraFactory;
			}
		}
		
		return factory;
	}
	
}
