package es.edu.alten.practica0.modelo.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.edu.alten.practica0.modelo.Lagarto;
import es.edu.alten.practica0.modelo.Papel;
import es.edu.alten.practica0.modelo.Piedra;
import es.edu.alten.practica0.modelo.PiedraPapelTijeraFactory;
import es.edu.alten.practica0.modelo.Spock
;
import es.edu.alten.practica0.modelo.Tijera;

class PiedraPapelTijeraFactoryTest {

	PiedraPapelTijeraFactory piedra;
	PiedraPapelTijeraFactory papel;
	PiedraPapelTijeraFactory tijera;
	PiedraPapelTijeraFactory lagarto;
	PiedraPapelTijeraFactory spock;
	
	@BeforeEach
	void setUp() throws Exception {
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
		lagarto = new Lagarto();
		spock = new Spock();
	}
	
	@AfterEach
	void tearDown() throws Exception {
		piedra = null;
		papel = null;
		tijera = null;
		lagarto = null;
		spock = null;
	}

	@Test
	void testGetInstancePiedra() {
		Assertions.assertEquals("", "");
		Assertions.assertEquals("piedra", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA).getNombre().toLowerCase());
	}
	
	@Test
	void testGetInstancePapel() {
		Assertions.assertEquals("papel", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL).getNombre().toLowerCase());
	}
	
	@Test
	void testGetInstanceTijera() {
		Assertions.assertEquals("tijera", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA).getNombre().toLowerCase());
	}
	
	@Test
	void testGetInstanceLagarto() {
		Assertions.assertEquals("lagarto", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.LAGARTO).getNombre().toLowerCase());
	}
	
	@Test
	void testGetInstanceSpock() {
		Assertions.assertEquals("spock", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.SPOCK).getNombre().toLowerCase());
	}
	
	@Test
	void testCompararPiedraGana() {
		Assertions.assertEquals(1,piedra.comparar(tijera));
	}
	
	@Test
	void testCompararPapelGana() {
		Assertions.assertEquals(1,papel.comparar(piedra));
	}
	
	@Test
	void testCompararTijeraGana() {
		Assertions.assertEquals(1,tijera.comparar(papel));
	}

	
	@Test
	void testCompararPiedraPierde() {
		Assertions.assertEquals(-1,piedra.comparar(papel));
	}
	
	@Test
	void testCompararPapelPierde() {
		Assertions.assertEquals(-1,papel.comparar(tijera));
	}
	
	@Test
	void testCompararTijeraPierde() {
		Assertions.assertEquals(-1,tijera.comparar(piedra));
	}
	
	@Test
	void testEmpataPiedra() {
		Assertions.assertEquals(0,piedra.comparar(piedra));
	}
	
	@Test
	void testEmpataPapel() {
		Assertions.assertEquals(0,papel.comparar(papel));
	}
	
	@Test
	void testEmpataTijera() {
		Assertions.assertEquals(0,tijera.comparar(tijera));
	}
	
	@Test
	void testCompararLagartoPierde() {
		Assertions.assertEquals(-1,lagarto.comparar(tijera));
	}
	
	@Test
	void testCompararLagartoGana() {
		Assertions.assertEquals(1,lagarto.comparar(spock));
	}
	
	@Test
	void testCompararSpockPierde() {
		Assertions.assertEquals(-1,spock.comparar(papel));
	}
	
	@Test
	void testCompararSpockGana() {
		Assertions.assertEquals(1,spock.comparar(tijera));
	}
	
	@Test
	void testEmpataLagarto() {
		Assertions.assertEquals(0,lagarto.comparar(lagarto));
	}
	
	@Test
	void testEmpataSpock() {
		Assertions.assertEquals(0,spock.comparar(spock));
	}

}
