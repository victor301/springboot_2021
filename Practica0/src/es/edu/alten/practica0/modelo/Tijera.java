package es.edu.alten.practica0.modelo;

public class Tijera extends PiedraPapelTijeraFactory{

	private Tijera(String nombre, int numero) {
		super("tijera", TIJERA);
	}
	
	public Tijera(String nombre) {
		super(nombre, OptionsDefined.TIJERA.getId());
	}
	
	public Tijera() {
		this(OptionsDefined.TIJERA.getName(),OptionsDefined.TIJERA.getId());
	}

	@Override
	public boolean isMe(int numero) {
		return TIJERA == numero;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory piedraPapelTijeraFactory) {
		int result = 0;
		
		switch (piedraPapelTijeraFactory.getNumero()) {
		
		case PAPEL:
		case LAGARTO:
			result = 1;
			descripcionResultado = "Tijera le gano a " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case PIEDRA:
		case SPOCK:
			result = -1;
			descripcionResultado = "Tijera perdio contra " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case TIJERA:
			result = 0;
			descripcionResultado = "Tijera empato contra " + piedraPapelTijeraFactory.getNombre();
			break;
			
		}
		
		return result;
	}

}
