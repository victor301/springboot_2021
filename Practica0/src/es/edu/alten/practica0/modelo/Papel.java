package es.edu.alten.practica0.modelo;

public class Papel extends PiedraPapelTijeraFactory{

	private Papel(String nombre, int numero) {
		super("papel", PAPEL);
	}
	
	public Papel(String nombre) {
		this(nombre,OptionsDefined.PAPEL.getId());
	}
	
	public Papel() {
		this(OptionsDefined.PAPEL.getName(),OptionsDefined.PAPEL.getId());
	}

	@Override
	public boolean isMe(int numero) {
		return PAPEL == numero;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory piedraPapelTijeraFactory) {
		int result = 0;
		
		switch (piedraPapelTijeraFactory.getNumero()) {
		
		case PIEDRA:
		case SPOCK:
			result = 1;
			descripcionResultado = "Papel le gano a " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case TIJERA:
		case LAGARTO:
			result = -1;
			descripcionResultado = "Papel perdio contra " + piedraPapelTijeraFactory.getNombre();
			break;
			
		case PAPEL:
			result = 0;
			descripcionResultado = "Piedra empato contra " + piedraPapelTijeraFactory.getNombre();
		}
		
		return result;
	
	}

}
