package com.victorCode.service;

import java.util.List;

import com.victorCode.dto.PersonaDto;

public interface IService {

	List<PersonaDto> list();
	
	void insert(PersonaDto p);
	
	void delete(PersonaDto p);
}
