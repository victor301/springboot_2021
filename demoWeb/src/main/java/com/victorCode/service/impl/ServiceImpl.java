package com.victorCode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.victorCode.dto.PersonaDto;
import com.victorCode.entity.PersonaEntity;
import com.victorCode.repo.IPersonaRepo;
import com.victorCode.service.IService;

@Service
public class ServiceImpl implements IService {

	@Autowired
	IPersonaRepo iPersonaRepo;
	
	@Override
	public List<PersonaDto> list() {
		return PersonaEntity.toPersonaDto(iPersonaRepo.findAll());
	}

	@Override
	public void insert(PersonaDto p) {
		iPersonaRepo.save(PersonaDto.toPersonaEntity(p));
	}

	@Override
	public void delete(PersonaDto p) {
		iPersonaRepo.delete(PersonaDto.toPersonaEntity(p));
	}

}
