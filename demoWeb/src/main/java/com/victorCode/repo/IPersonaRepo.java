package com.victorCode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.victorCode.entity.PersonaEntity;

public interface IPersonaRepo extends JpaRepository<PersonaEntity, Integer>{

}
