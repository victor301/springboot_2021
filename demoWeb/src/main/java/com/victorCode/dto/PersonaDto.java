package com.victorCode.dto;

import java.util.ArrayList;
import java.util.List;

import com.victorCode.entity.PersonaEntity;

public class PersonaDto {

	private int codigo;
	private String nombre;
	
	public PersonaDto() {
		this.codigo = 0;
		this.nombre = "";
	}
	
	public PersonaDto(int codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}
	
	public PersonaDto(PersonaEntity p) {
		this.codigo = p.getCodigo();
		this.nombre = p.getNombre();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static PersonaEntity toPersonaEntity(PersonaDto p) {
		return new PersonaEntity(p);
	}
	
	public static List<PersonaEntity> toPersonaEntity(List<PersonaDto> personas) {
		List<PersonaEntity> result = new ArrayList<PersonaEntity>();
		
		for (PersonaDto p : personas) {
			result.add(toPersonaEntity(p));
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		return "PersonaDto [codigo=" + codigo + ", nombre=" + nombre + "]";
	}
	
}

