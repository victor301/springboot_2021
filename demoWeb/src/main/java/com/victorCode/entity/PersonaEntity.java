package com.victorCode.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.victorCode.dto.PersonaDto;

@Entity(name = "PERSONAS")
public class PersonaEntity {

	@Id
	@Column(name="CODIGO")
	private int codigo;
	@Column(name="NOMBRE")
	private String nombre;

	public PersonaEntity() {
		super();
		this.codigo = 0;
		this.nombre = "";
	}
	
	public PersonaEntity(int codigo, String nombre) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
	}
	
	public PersonaEntity(PersonaDto p) {
		this.codigo = p.getCodigo();
		this.nombre = p.getNombre();
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static PersonaDto toPersonaDto(PersonaEntity p) {
		return new PersonaDto(p);
	}
	
	public static List<PersonaDto> toPersonaDto(List<PersonaEntity> personas) {
		List<PersonaDto> result = new ArrayList<PersonaDto>();
		
		for (PersonaEntity p : personas) {
			result.add(toPersonaDto(p));
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		return "Persona [codigo=" + codigo + ", nombre=" + nombre + "]";
	}
	
	
}
