package com.victorCode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.victorCode.dto.PersonaDto;
import com.victorCode.service.IService;

@RestController
public class RestDemoController {

	@Autowired
	IService iService;
	
	@GetMapping("/list")
	public List<PersonaDto> listar() {
		return iService.list();
	}
	
	@PutMapping("/insert")
	public void insert(@RequestBody PersonaDto p) {
		System.out.println(p.toString());
		iService.insert(p);
	}
	
	@DeleteMapping("/delete")
	public void delete(@RequestBody PersonaDto p) {
		iService.delete(p);
	}
}
