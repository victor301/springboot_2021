package com.victorCode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.victorCode.entity.PersonaEntity;
import com.victorCode.repo.IPersonaRepo;

@Controller
public class GreetingController {

	@Autowired
	IPersonaRepo iPersonaRepo;
	
	@GetMapping("/greeting")
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		model.addAttribute("name", name);
		
		PersonaEntity p = new PersonaEntity(1, "Victor");
		iPersonaRepo.save(p);
		
		return "greeting";
	}

}