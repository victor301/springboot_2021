package com.example.demo.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.demo.repository.IPersonaRepository;

@Service
public class PerosnaRepoImpl implements IPersonaRepository {

	private static Logger LOG = LoggerFactory.getLogger(PerosnaRepoImpl.class);
	
	@Override
	public void registrar(String name) {
		// TODO Auto-generated method stub
		LOG.info(name + " registrado correctamente");
	}

}
