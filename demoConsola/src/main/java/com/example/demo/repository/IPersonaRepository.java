package com.example.demo.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface IPersonaRepository {

	void registrar(String name);
}
