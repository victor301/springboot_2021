package com.example.demo.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.demo.repository.IPersonaRepository;
import com.example.demo.services.IPersonaService;

@Service
@Qualifier("persona")
public class IPersonaServiceImpl implements IPersonaService{

	private static Logger LOG = LoggerFactory.getLogger(IPersonaServiceImpl.class);
	
	@Autowired
	private IPersonaRepository iPersonaRepository;
	
	@Override
	public void registrar(String name) {
		// TODO Auto-generated method stub
		LOG.info("se registra a " + name);
		iPersonaRepository.registrar(name);
	}

}
