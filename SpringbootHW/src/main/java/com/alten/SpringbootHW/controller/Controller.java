package com.alten.SpringbootHW.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alten.SpringbootHW.dto.Response;
import com.alten.SpringbootHW.service.IService;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	IService iService;
	
	@GetMapping("/helloWorld")
	public String helloWorld() {
		return iService.helloWorld();
	}
	
	@GetMapping("/helloObject")
	public Response helloObject() {
		return new Response();
	}
	
	@GetMapping("/greeting")
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		model.addAttribute("name", name);
		return "greeting";
	}

}
