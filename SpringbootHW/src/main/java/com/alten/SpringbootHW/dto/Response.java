package com.alten.SpringbootHW.dto;

public class Response {

	private String response;
	
	public Response() {
		this.response = "Hola mundo";
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	
}
