package com.victor.alumno.dao;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.victor.alumno.model.Alumno;

class AlumnoDAOTest {
	
	ApplicationContext context;
	AlumnoDAO alumnoDAO;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("Spring-Module.xml");
		alumnoDAO = (AlumnoDAO) context.getBean("alumnoDAO");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testInsert() {

        Alumno alumno = new Alumno("testApellido","testNombre","testEmail","testConocimientos","testGit","testObservaciones");
		int result = alumnoDAO.insert(alumno);
        
        Assertions.assertEquals(1, result);
        
	}
	
	@Test
	void testUpdate() {
		List<Alumno> alumnos = alumnoDAO.list(new Alumno());
		int result = 0;
		for (Alumno a : alumnos) {
			if(a.getAluApellido().equals("testApellido")) {
				a.setAluConocimientos("conocimientos update");
				result = alumnoDAO.update(a);
			}
		}
		
		Assertions.assertEquals(1, result);
	}
	
	@Test
	void testDelete() {
		List<Alumno> alumnos = alumnoDAO.list(new Alumno());
		int result = 0;
		for (Alumno a : alumnos) {
			if(a.getAluApellido().equals("testApellido")) {
				result = alumnoDAO.delete(a);
			}
		}
		
		Assertions.assertEquals(1, result);
				
	}
	
	@Test
	void testListById() {
		
		Alumno alumno = new Alumno();
		alumno.setAluId(3);
		
		List<Alumno> alumnos = alumnoDAO.list(alumno);
		
		Assertions.assertEquals(3, alumnos.get(0).getAluId());
				
	}

}
