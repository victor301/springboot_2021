package com.victor.alumno.dao;

import java.text.SimpleDateFormat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;

import com.victor.alumno.model.Auditoria;
import com.victor.alumno.model.Jugada;
import com.victor.alumno.model.Jugador;


@ContextConfiguration
public class JugadorTest {

	ClassPathXmlApplicationContext context;
	Jugador j1;
	
	ClassPathXmlApplicationContext context2;
	Jugador j2;
	
	ClassPathXmlApplicationContext context3;
	Jugada jugada;
	
	ClassPathXmlApplicationContext context4;
	Auditoria auditoria;

	@BeforeEach
	void setUp() throws Exception{
		context = new ClassPathXmlApplicationContext("/META-INF/juego/Jugador.xml");
		j1 = context.getBean(Jugador.class);
		
		context2 = new ClassPathXmlApplicationContext("/META-INF/juego/Jugador2.xml");
		j2 = context2.getBean(Jugador.class);
		
		context3 = new ClassPathXmlApplicationContext("/META-INF/juego/Jugada.xml");
		jugada = context3.getBean(Jugada.class);
		
		context4 = new ClassPathXmlApplicationContext("/META-INF/juego/Jugada2.xml");
		auditoria = context4.getBean(Auditoria.class);
	}
	
	@AfterEach
	void tearDown() throws Exception {
		context = null;
		j1 = null;
		
		context2 = null;
		j2 = null;
		
		context3 = null;
		jugada = null;
		
		context4 = null;
		auditoria = null;
	}
	
	@Test
	void testGetBeanAttributes() {
		Assertions.assertEquals(1, j1.getCodigo());
		Assertions.assertEquals("j1", j1.getNombre());
		Assertions.assertEquals("j1 nick", j1.getNick());
		Assertions.assertEquals("PIEDRA", j1.getJugadaElegida().getNombre());
	}
	
	@Test
	void testGetBeanAttributes2() {
		Assertions.assertEquals(2, j2.getCodigo());
		Assertions.assertEquals("j2", j2.getNombre());
		Assertions.assertEquals("j2 nick", j2.getNick());
		Assertions.assertEquals("SPOCK", j2.getJugadaElegida().getNombre());
	}
	
	@Test
	void testContarDespuesDeLaJugada() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Assertions.assertEquals("1996-02-22", sdf.format(jugada.getFechaHora()));
	}
	
	@Test
	void testGetBeanAttributesJugada() {
		System.out.println(auditoria.toString());
		jugada.getDescripcionDelResultado();
		jugada.getDescripcionDelResultado();
		jugada.getDescripcionDelResultado();
		
		Assertions.assertEquals(0, auditoria.getCantidadJugadas());
	}
}