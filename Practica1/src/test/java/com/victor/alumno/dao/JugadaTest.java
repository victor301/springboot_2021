package com.victor.alumno.dao;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.victor.alumno.model.Auditoria;
import com.victor.alumno.model.IJugada;
import com.victor.alumno.model.Jugada;
import com.victor.alumno.model.Jugador;

import es.edu.alten.practica0.modelo.Piedra;
import es.edu.alten.practica0.modelo.Spock;
import oracle.net.aso.j;
import sia.knights.Knight;

public class JugadaTest {
	
	Jugada jugada;
	
	ClassPathXmlApplicationContext context;
	IJugada iJugada;
	Auditoria auditoria;

	@BeforeEach
	void setUp() throws Exception{
		Jugador j1 = new Jugador(1, "jugador 1", "nick j1", new Piedra());
		Jugador j2 = new Jugador(2, "jugador 2", "nick j2", new Spock());
		jugada = new Jugada(1, new Date(), j1, j2);
		
		context = new ClassPathXmlApplicationContext("/META-INF/juego/Jugada2.xml");
		iJugada = context.getBean(IJugada.class);
		auditoria = context.getBean(Auditoria.class);
	}
	
	@AfterEach
	void tearDown() throws Exception {
		jugada = null;
	}
	
	@Test
	void testGetDescripcionResultado() {
		Assertions.assertEquals("Piedra perdio contra SPOCK", jugada.getDescripcionDelResultado());
	}
	
	@Test
	void testGetBeanAttributesJugada() {
		System.out.println(auditoria.toString());
		iJugada.getDescripcionDelResultado();
		
		Assertions.assertEquals(3, auditoria.getCantidadJugadas());
	}
}
