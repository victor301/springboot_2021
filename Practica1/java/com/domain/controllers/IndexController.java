package com.domain.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.victor.alumno.dao.AlumnoDAO;
import com.victor.alumno.dao.impl.JdbcAlumnoDAO;
import com.victor.alumno.model.Alumno;

import es.edu.alten.practica0.modelo.OptionsDefined;
import es.edu.alten.practica0.modelo.PiedraPapelTijeraFactory;

@Controller
public class IndexController {

	@RequestMapping("/")
	public String goPresentacion() {
		return "/Presentacion";
	}
	
	@RequestMapping("/home")
	public String goHome() {
		return "/Index";
	}
	
	@RequestMapping("/listado")
	public String goListado(Model model) {
		
		ApplicationContext context =
                new ClassPathXmlApplicationContext("Spring-Module.xml");

		AlumnoDAO alumnoDAO = (AlumnoDAO) context.getBean("alumnoDAO");
		//(AlumnoDAO alumnoDAO2 = (AlumnoDAO) context.getBean(JdbcAlumnoDAO.class);
        List<Alumno> alumnos = alumnoDAO.list(new Alumno());
        
		model.addAttribute("titulo", "listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos", alumnos);
		
		return "/Listado";
	}
	
	@RequestMapping("/play")
	public String jugar(Model model) {
		Map<Integer, String> optionsMap = new HashMap<Integer, String>();
		
		optionsMap.put(OptionsDefined.PIEDRA.getId(), OptionsDefined.PIEDRA.getName());
		optionsMap.put(OptionsDefined.PAPEL.getId(), OptionsDefined.PAPEL.getName());
		optionsMap.put(OptionsDefined.TIJERA.getId(), OptionsDefined.TIJERA.getName());
		optionsMap.put(OptionsDefined.LAGARTO.getId(), OptionsDefined.LAGARTO.getName());
		optionsMap.put(OptionsDefined.SPOCK.getId(), OptionsDefined.SPOCK.getName());
		
		model.addAttribute("options",optionsMap);
		
		return "MainGame";
	}
	
	@RequestMapping("/checkResult")
	public String checkResult(@RequestParam("options")int option, Model model) {
		
		PiedraPapelTijeraFactory papelTijeraFactoryGame;
		PiedraPapelTijeraFactory papelTijeraFactoryUser;
		
		int value = 0;
		
		while  (value == 0) {
			value = (int) (Math.random() * 6);
		}
		
		//Math.random()*100%5 +1 solucion miguel para seleccionar el numero mas efeciva
		
		papelTijeraFactoryGame = PiedraPapelTijeraFactory.getInstance(value);
		papelTijeraFactoryUser = PiedraPapelTijeraFactory.getInstance(option);
		
		String machine = "valor maquina  " + papelTijeraFactoryGame.getNombre();
		String player = "valor jugador  " + papelTijeraFactoryUser.getNombre();
		
		papelTijeraFactoryUser.comparar(papelTijeraFactoryGame);
		
		String response = "";
		
		model.addAttribute("player", player);
		model.addAttribute("machine", machine);
		model.addAttribute("result", papelTijeraFactoryUser.getDescripcionResultado());
		
		return "EndGame";
	}
	
}
