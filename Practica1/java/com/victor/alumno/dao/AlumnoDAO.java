package com.victor.alumno.dao;

import java.util.List;

import com.victor.alumno.model.Alumno;

public interface AlumnoDAO {

    int insert(Alumno alumno);
    
    List<Alumno> list(Alumno alumno);
    
    int update(Alumno alumno);
    
    int delete(Alumno alumno);

}
