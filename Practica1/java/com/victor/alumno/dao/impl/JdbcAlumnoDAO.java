package com.victor.alumno.dao.impl;

import com.victor.alumno.dao.AlumnoDAO;
import com.victor.alumno.model.Alumno;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcAlumnoDAO implements AlumnoDAO {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public int insert(Alumno alu) {

        String sql = "INSERT INTO alumnos " +
                "(ALU_APELLIDO, ALU_NOMBRE, ALU_EMAIL, ALU_CONOCIMIENTOS, ALU_GIT, ALU_OBSERVACIONES) VALUES (?, ?, ?, ?, ?, ?)";
        Connection conn = null;
        int result = 0;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, alu.getAluApellido());
            ps.setString(2, alu.getAluNombre());
            ps.setString(3, alu.getAluEmail());
            ps.setString(4, alu.getAluConocimientos());
            ps.setString(5, alu.getAluGit());
            ps.setString(6, alu.getAluObservaciones());
            result = ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            System.out.println("error " + e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                	System.out.println("error " + e);
                }
            }
        }
        
        return result;

    }

	@Override
	public List<Alumno> list(Alumno a) {
		StringBuilder sql = new StringBuilder("SELECT * FROM ALUMNOS");
        Connection conn = null;
        List<Alumno> alumnos = new ArrayList<Alumno>();

        if (a.getAluId() > 0) {
        	sql.append(" WHERE ALU_ID = ?");
        }
        
        try {
        	conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql.toString());
            if (a.getAluId() > 0) {
            	ps.setInt(1, a.getAluId());
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
            	Alumno alu = new Alumno(
                        rs.getInt("ALU_ID"),
                        rs.getString("ALU_APELLIDO"),
                        rs.getString("ALU_NOMBRE"),
                        rs.getString("ALU_EMAIL"),
                        rs.getString("ALU_CONOCIMIENTOS"),
                        rs.getString("ALU_GIT"),
                        rs.getString("ALU_OBSERVACIONES")
                );
            	alumnos.add(alu);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
        	System.out.println("error " + e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                	System.out.println("error " + e);
                }
            }
        }
		return alumnos;
	}

	@Override
	public int update(Alumno alu) {
		String sql = "UPDATE alumnos set ALU_APELLIDO = ?, ALU_NOMBRE = ?, ALU_EMAIL = ?, ALU_CONOCIMIENTOS = ?, ALU_GIT = ?, ALU_OBSERVACIONES = ? WHERE ALU_ID = ?";
        Connection conn = null;
        int result = 0;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, alu.getAluApellido());
            ps.setString(2, alu.getAluNombre());
            ps.setString(3, alu.getAluEmail());
            ps.setString(4, alu.getAluConocimientos());
            ps.setString(5, alu.getAluGit());
            ps.setString(6, alu.getAluObservaciones());
            ps.setInt(7, alu.getAluId());
            result = ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                	System.out.println("error " + e);
                }
            }
        }
		
        return result;
        
	}

	@Override
	public int delete(Alumno alu) {
		String sql = "DELETE FROM alumnos WHERE ALU_ID = ?";
        Connection conn = null;
        int result = 0;

        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, alu.getAluId());
            result = ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                	System.out.println("error " + e);
                }
            }
        }
        
        return result;
		
	}

}
