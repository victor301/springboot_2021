package com.victor.alumno.model;

import java.util.Objects;

import es.edu.alten.practica0.modelo.PiedraPapelTijeraFactory;

public class Jugador {

	private int codigo;
	private String nombre;
	private String nick;
	private PiedraPapelTijeraFactory jugadaElegida;
	
	public Jugador() {
		this.codigo = 0;
		this.nombre = "";
		this.nick = "";
		this.jugadaElegida = PiedraPapelTijeraFactory.getInstance(1);
	}
	
	public Jugador(int codigo, String nombre, String nick, PiedraPapelTijeraFactory jugadaElegida) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.nick = nick;
		this.jugadaElegida = jugadaElegida;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public PiedraPapelTijeraFactory getJugadaElegida() {
		return jugadaElegida;
	}

	public void setJugadaElegida(PiedraPapelTijeraFactory jugadaElegida) {
		this.jugadaElegida = jugadaElegida;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo, jugadaElegida, nick, nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jugador other = (Jugador) obj;
		return codigo == other.codigo && Objects.equals(jugadaElegida, other.jugadaElegida)
				&& Objects.equals(nick, other.nick) && Objects.equals(nombre, other.nombre);
	}

	@Override
	public String toString() {
		return "Jugador [codigo=" + codigo + ", nombre=" + nombre + ", nick=" + nick + ", jugadaElegida="
				+ jugadaElegida + "]";
	}
	
	
}
