package com.victor.alumno.model;

public class Alumno {

    private int aluId;
    private String aluApellido;
    private String aluNombre;
    private String aluEmail;
    private String aluConocimientos;
    private String aluGit;
    private String aluObservaciones;


    public Alumno() {
        this.aluId = 0;
        this.aluApellido = "unknown";
        this.aluNombre = "unknown";
        this.aluEmail = "unknown";
        this.aluConocimientos = "unknown";
        this.aluGit = "unknown";
        this.aluObservaciones = "unknown";
    }

    /**
     * el id no es necesario ya que se utiliza la sequencia alu_seq para darle valor con el trigger alu_id_seq
     * @param aluApellido
     * @param aluNombre
     * @param aluEmail
     * @param aluConocimientos
     * @param aluGit
     * @param aluObservaciones
     */
    public Alumno(String aluApellido, String aluNombre, String aluEmail, String aluConocimientos, String aluGit, String aluObservaciones) {
        this.aluId = 0;
        this.aluApellido = aluApellido;
        this.aluNombre = aluNombre;
        this.aluEmail = aluEmail;
        this.aluConocimientos = aluConocimientos;
        this.aluGit = aluGit;
        this.aluObservaciones = aluObservaciones;
    }

    public Alumno(int aluId, String aluApellido, String aluNombre, String aluEmail, String aluConocimientos, String aluGit, String aluObservaciones) {
        this.aluId = aluId;
        this.aluApellido = aluApellido;
        this.aluNombre = aluNombre;
        this.aluEmail = aluEmail;
        this.aluConocimientos = aluConocimientos;
        this.aluGit = aluGit;
        this.aluObservaciones = aluObservaciones;
    }

    public int getAluId() {
        return aluId;
    }

    public void setAluId(int aluId) {
        this.aluId = aluId;
    }

    public String getAluApellido() {
        return aluApellido;
    }

    public void setAluApellido(String aluApellido) {
        this.aluApellido = aluApellido;
    }

    public String getAluNombre() {
        return aluNombre;
    }

    public void setAluNombre(String aluNombre) {
        this.aluNombre = aluNombre;
    }

    public String getAluEmail() {
        return aluEmail;
    }

    public void setAluEmail(String aluEmail) {
        this.aluEmail = aluEmail;
    }

    public String getAluConocimientos() {
        return aluConocimientos;
    }

    public void setAluConocimientos(String aluConocimientos) {
        this.aluConocimientos = aluConocimientos;
    }

    public String getAluGit() {
        return aluGit;
    }

    public void setAluGit(String aluGit) {
        this.aluGit = aluGit;
    }

    public String getAluObservaciones() {
        return aluObservaciones;
    }

    public void setAluObservaciones(String aluObservaciones) {
        this.aluObservaciones = aluObservaciones;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "aluId=" + aluId +
                ", aluApellido='" + aluApellido + '\'' +
                ", aluNombre='" + aluNombre + '\'' +
                ", aluEmail='" + aluEmail + '\'' +
                ", aluConocimientos='" + aluConocimientos + '\'' +
                ", aluGit='" + aluGit + '\'' +
                ", aluObservaciones='" + aluObservaciones + '\'' +
                '}';
    }
}
