package com.victor.alumno.model;

import java.util.Date;

public class Auditoria {

	private Date fecha;
	private int cantidadJugadas;
	
	public Auditoria() {
		this.fecha = new Date();
		this.cantidadJugadas = 0;
	}
	
	public Auditoria(Date fecha, int cantidadJugadas) {
		this.fecha = fecha;
		this.cantidadJugadas = cantidadJugadas;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getCantidadJugadas() {
		return cantidadJugadas;
	}

	public void setCantidadJugadas(int cantidadJugadas) {
		this.cantidadJugadas = cantidadJugadas;
	}
	
	public void contarAntesDeLaJugada() {
		this.cantidadJugadas = 3;
		System.out.println("antes");
	}
	
	public void contarDespuesDeLaJugada() {
		this.cantidadJugadas = 3;
		System.out.println("despues");
	}

	@Override
	public String toString() {
		return "Auditoria [fecha=" + fecha + ", cantidadJugadas=" + cantidadJugadas + "]";
	}
	
	
	
}
