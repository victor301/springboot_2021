package com.victor.alumno.model;

import java.util.Date;
import java.util.Objects;

import sia.knights.Knight;

public class Jugada implements IJugada {
	private Integer codigo;
	private Date fechaHora;
	private Jugador j1;
	private Jugador j2;
	
	public Jugada() {
		this.codigo = 0;
		this.fechaHora = new Date();
		this.j1 = new Jugador();
		this.j2 = new Jugador();
	}
	
	public Jugada(int codigo, Date fechaHora, Jugador j1, Jugador j2) {
		this.codigo = codigo;
		this.fechaHora = fechaHora;
		this.j1 = j1;
		this.j2 = j2;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Jugador getJ1() {
		return j1;
	}

	public void setJ1(Jugador j1) {
		this.j1 = j1;
	}

	public Jugador getJ2() {
		return j2;
	}

	public void setJ2(Jugador j2) {
		this.j2 = j2;
	}
	
	@Override
	public String getDescripcionDelResultado() {
		
		j1.getJugadaElegida().comparar(j2.getJugadaElegida());
		
		return j1.getJugadaElegida().getDescripcionResultado();
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(codigo, fechaHora, j1, j2);
	}

	@Override
	public String toString() {
		return "Jugada [codigo=" + codigo + ", fechaHora=" + fechaHora + ", j1=" + j1.toString() + ", j2=" + j2.toString() + "]";
	}

}

