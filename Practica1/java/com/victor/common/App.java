package com.victor.common;

import sia.knights.BraveKnight;
import sia.knights.Knight;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App
{
    public static void main( String[] args )
    {
//    	ApplicationContext context =
//                new ClassPathXmlApplicationContext("Spring-Module.xml");
//    	Knight knight = context.getBean(BraveKnight.class);
//    	knight.embarkOnQuest();
    	
//    	ApplicationContext context =
//                new ClassPathXmlApplicationContext("META-INF/spring/knight.xml");
//Knight knightDamsel = context.getBean(BraveKnight.class);
//    	
//
//    	knightDamsel.embarkOnQuest();

    	
    	ApplicationContext context =
                new ClassPathXmlApplicationContext("META-INF/spring/minstrel.xml");

    	Knight knight = context.getBean(BraveKnight.class);
    	
    	knight.embarkOnQuest();
    	

    }
}
